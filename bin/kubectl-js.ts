#!/usr/bin/env ts-node
const commander = require('commander');
const packageJson = require('../package.json');

commander
  .version(packageJson.version)
  .description(packageJson.description)
  .command('template <path> [options]', 'outputs the YAML for the given JS')
  .parse(process.argv);
