#!/usr/bin/env ts-node
import * as fs from 'fs';
import * as path from 'path';
import * as yaml from 'yaml';
const commander = require('commander');
const packageJson = require('../package.json');

commander
  .version(packageJson.version)
  .description(packageJson.description)
  .arguments('<path>')
  .option('-o, --output <json|yaml>', 'specify either json or yaml', 'yaml')
  .action(function(...args) {
    const parsedArgs = commander.parseExpectedArgs(args);
    const outputFormat = parsedArgs.output;
    const templatePath = path.resolve(parsedArgs.args[0]);
    try {
      const templateInfo = fs.lstatSync(templatePath);
      if (!templateInfo.isFile()) {
        throw new Error(`'${templatePath}' does not seem to be a file`)
      }
      const template = require(templatePath);
      if (outputFormat === 'yaml') {
        process.stdout.write(yaml.stringify(template, {
          anchorPrefix: 'anchor',
        }));
      } else if (outputFormat === 'json') {
        process.stdout.write(JSON.stringify(template, null, 2));
      } else {
        throw new Error(`the specified format '${outputFormat}' is invalid - try 'json' or 'yaml'`)
      }
    } catch (ex) {
      console.error(ex);
    }
  })
  .parse(process.argv)

if (commander.args.length === 0) {
  commander.outputHelp();
}
