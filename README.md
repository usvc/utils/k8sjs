# `k8sjs`

A package for applying JS templated Kubernetes manifests created with [`k8sresources`](https://www.npmjs.com/package/k8sresources).

# Usage

## Installation

```sh
npm install --global k8sjs k8sresources;
```

## Applying a JS file template

You will need `kubectl` on your machine before this will work. To check if the plugin was installed successfully, use `kubectl plugin list`.

To test a deployment, create a file with the following contents:

```js
const {createDeployment} = require('k8sresources');

const deployment = createDeployment('apps/v1', {name: 'test'});
deployment.spec.template.spec.containers.push(
  {
    image: 'zephinzer/demo-echoserver:latest',
    imagePullPolicy: 'IfNotPresent',
    name: 'echoserver',
  },
);

module.exports = deployment;
```

Then run:

```sh
kubectl js template ./yourfile.js | kubectl apply -f -
# or for typescript files...
kubectl ts template ./yourfile.ts | kubectl apply -f -
```

To just view the templated output, avoid applying it:

```sh
kubectl js template ./yourfile.js
# or for typescript files...
kubectl ts template ./yourfile.ts
```

# Development Runbook

## Development

1. Install dependencies with `npm install`
2. Run `npm link` to link the binaries
3. Open a separate terminal and run `npm run dev` to start the file watcher/compiler
4. Run `kubectl js template` to test your changes (wait for `tsc --project .` to complete running first)

## Publishing

1. Bump the version in the `package.json`
2. Build the project using `npm run build`
3. Commit all changes and push to the Git remote
4. Run `npm publish` to publish to the NPM registry

# License

This code is licensed under [the MIT license](./LICENSE).
